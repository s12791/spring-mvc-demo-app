/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.mvc;

import com.mycompany.spring.mvc.model.Person;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Karolina
 */
public interface PersonDAO extends CrudRepository<Person, Integer>{
    
}
