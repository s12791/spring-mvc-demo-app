<%-- 
    Document   : add
    Created on : 2017-02-24, 15:59:58
    Author     : Karolina
--%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Adding the Person</title>
    </head>
    <body>
        <h1>Add the Person</h1>
        <form:form commandName="person" action="save.htm" method="POST">
            <form:input path="name"/> <form:errors path="name"/><br/>
            <form:input path="lastName"/><form:errors path="lastName"/><br/>
            <form:input path="email"/><form:errors path="email"/><br/>
            <form:input path="discount"/><form:errors path="discount"/><br/>
            <form:button>Save</form:button>
        </form:form>
        
    </body>
</html>
